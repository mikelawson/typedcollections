<?php
/**
 * @author Mike Lawson <mike@thecommissioncafe.com>
 */

namespace Typed\Collections;
/*
 * sigh, i don’t get Lucca sometimes. it’s like “how come you don’t talk to me in person when we’re in the same room”, and i’m like I do, all the time, you’re just never listening and when you ask fivem inutes later what isaid, of course idon’t remember. she asked what i was working on right now and i told her one of my personal projects. she asked what and i told her, and she’s like “oh, what is that?”

last night i told her exactly what it was, and tonight she asks the same quetion so i  give the same explanation.

so given that i feel like it’s not that i don’t tal, its’ that she is never listening
 */
use Closure;

interface TypedCollection {

    public function getTypeStr() : string;

    public function testType(TypedCollection $typedCollection) : bool;

    public function compare(TypedCollection $typedCollection, Closure $closure) : int;

    public function sort(Closure $closure) : TypedCollection;

    public function map(Closure $callback) : TypedCollection;

    public function keys() : array;

    public function values() : TypedCollection;

    /**
     * @param string|int $index
     * @return mixed
     */
    public function get($index);

    /**
     * @param string|int $index
     * @return mixed
     */
    public function set($index);

    public function filter(Closure $callback);

    /**
     * @param string|int $index
     * @return mixed
     */
    public function replace($index);

    /**
     * @param TypedCollection $typedCollection
     * @param string|int $startIndex
     * @return mixed
     */
    public function replaceAll(TypedCollection $typedCollection, $startIndex);

    /**
     * @param TypedCollection $typedCollection
     * @param string|int $startIndex
     * @return TypedCollection
     */
    public function replaceAllIntoNew(TypedCollection $typedCollection, $startIndex) : TypedCollection;



}