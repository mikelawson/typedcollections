<?php
/**
 * @author Mike Lawson <mike@thecommissioncafe.com>
 */

namespace Typed\Exceptions;

use Exception;

abstract class TypedException extends Exception {


}